import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lite_note/screens/dashboard.dart';
import 'package:lite_note/screens/notes_list.dart';

import 'location_test.dart';

class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final pageViewController = PageController();

    @override
    void dispose(){
      pageViewController.dispose();
    }

    return Scaffold(
      body: PageView(
        controller: pageViewController,
        children: [
          DashBoard(),
          NotesList(),
          LocationTest(),
        ],
      ),
      bottomNavigationBar: AnimatedBuilder(
          animation: pageViewController,
          builder: (context, snapshot) {
            return BottomNavigationBar(
              showUnselectedLabels: false,
              currentIndex: pageViewController?.page?.round() ?? 0,
              onTap: (index){
                pageViewController.animateToPage(index, duration: Duration(milliseconds: 300), curve: Curves.decelerate);
              },
              selectedItemColor: Theme.of(context).primaryColor,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.view_list_rounded),
                  label: 'Note list',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.playlist_add_sharp),
                  label: 'Add note',
                )
              ],
            );
          }
      ),
    );
  }
}
