import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lite_note/screens/notes_list.dart';

class DashBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('images/dashboard.jpg'),
          ),
        ],
      ),
    );
  }
}
