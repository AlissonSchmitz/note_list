import 'package:flutter/material.dart';
import 'package:lite_note/models/note.dart';
import 'package:lite_note/screens/widgets/note_item_list.dart';

class NotesList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notes List'),
      ),
      body: ListView.builder(
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) {
          return NoteItemList(
              new Note('Titulo $index', 'Subtitulo $index'));
        },
      ),
    );
  }
}
