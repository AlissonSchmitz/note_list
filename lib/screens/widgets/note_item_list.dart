import 'package:flutter/material.dart';
import 'package:lite_note/models/note.dart';

class NoteItemList extends StatelessWidget {

  Note _note;

  NoteItemList(this._note);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: CircleAvatar(
          child: Text('T'),
        ),
        title: Text(_note.title),
        subtitle: Text(_note.description),
      ),
    );
  }
}
