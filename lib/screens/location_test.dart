import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

import 'file:///C:/dev/lite_note/lib/models/user_location.dart';

class LocationTest extends StatelessWidget {
  UserLocation userLocation;
  Location location;

  LocationTest() {
    _init();
  }

  _init() async {
    location = new Location();
    await _serviceIsEnable();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location test'),
      ),
      body: Center(
        child: Text('Teste Alisson'),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.location_on_rounded),
        label: Text('Get location'),
        onPressed: () async {
          userLocation = await _getLocation();
          if (userLocation != null) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Geolocation teste Alisson'),
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: <Widget>[
                          Text('Latitude: ${userLocation.latitude}'),
                          Text('Longitude: ${userLocation.longitude}'),
                          Text('Speed: ${userLocation.speed}'),
                          Text('SpeedAccuracy: ${userLocation.speecAccuracy}'),
                          Text('Altitude: ${userLocation.altitude}'),
                          Text('Acurracy: ${userLocation.accuracy}'),
                          Text('Heading: ${userLocation.heading}'),
                          Text('Time: ${userLocation.time}'),
                        ],
                      ),
                    ),
                  );
                });
          }
        },
        backgroundColor: Colors.deepOrange,
      ),
    );
  }

  Future<UserLocation> _getLocation() async {
    if (await _serviceIsPermission()) {
      var teste = await location.getLocation();
      UserLocation _userLocation;
      try {
        _userLocation = new UserLocation(
          teste.latitude,
          teste.longitude,
          teste.speedAccuracy,
          teste.altitude,
          teste.speed,
          teste.accuracy,
          teste.time,
          teste.heading,
        );
        return _userLocation;
      } catch (e) {
        print('Could not get the location $e');
      }
    }
    return null;
  }

  _serviceIsEnable() async {
    var _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }
    return true;
  }

  Future<bool> _serviceIsPermission() async {
    var _servicePermission = await location.hasPermission();
    if (_servicePermission == PermissionStatus.DENIED) {
      _servicePermission = await location.requestPermission();
      if (_servicePermission != PermissionStatus.GRANTED) {
        return false;
      }
    }
    return true;
  }
}
