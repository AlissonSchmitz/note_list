class Note{
  String _title;
  String _description;

  Note(this._title, this._description);

  String get description => _description;

  String get title => _title;

  set description(String value) {
    _description = value;
  }

  set title(String value) {
    _title = value;
  }

  @override
  String toString() {
    return 'Note{_title: $_title, _description: $_description}';
  }
}